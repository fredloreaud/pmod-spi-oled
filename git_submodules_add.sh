#!/bin/bash
git submodule add https://gitlab.com/fredloreaud/osef-spi
git submodule add https://gitlab.com/fredloreaud/osef-gpio
git submodule add https://github.com/adafruit/Adafruit_SSD1306
git submodule add https://github.com/adafruit/Adafruit-GFX-Library
