#!/bin/bash
git submodule deinit -f osef-spi
git rm -f osef-spi
git rm --cached osef-spi
rm -rf .git/modules/osef-spi

git submodule deinit -f osef-gpio
git rm -f osef-gpio
git rm --cached osef-gpio
rm -rf .git/modules/osef-gpio

git submodule deinit -f Adafruit_SSD1306
git rm -f Adafruit_SSD1306
git rm --cached Adafruit_SSD1306
rm -rf .git/modules/Adafruit_SSD1306

git submodule deinit -f Adafruit-GFX-Library
git rm -f Adafruit-GFX-Library
git rm --cached Adafruit-GFX-Library
rm -rf .git/modules/Adafruit-GFX-Library

