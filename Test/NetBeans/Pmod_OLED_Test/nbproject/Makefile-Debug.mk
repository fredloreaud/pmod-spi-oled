#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/29dd86f/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../../NetBeans/Pmod_OLED/dist/Debug/GNU-Linux/libpmod_oled.a ../../../osef-spi/NetBeans/OSEF_SPI/dist/Debug/GNU-Linux/libosef_spi.a ../../../osef-gpio/NetBeans/OSEF_GPIO/dist/Debug/GNU-Linux/libosef_gpio.a ../../../osef-spi/osef-posix/NetBeans/OSEF_POSIX/dist/Debug/GNU-Linux/libosef_posix.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test: ../../../NetBeans/Pmod_OLED/dist/Debug/GNU-Linux/libpmod_oled.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test: ../../../osef-spi/NetBeans/OSEF_SPI/dist/Debug/GNU-Linux/libosef_spi.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test: ../../../osef-gpio/NetBeans/OSEF_GPIO/dist/Debug/GNU-Linux/libosef_gpio.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test: ../../../osef-spi/osef-posix/NetBeans/OSEF_POSIX/dist/Debug/GNU-Linux/libosef_posix.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pmod_oled_test ${OBJECTFILES} ${LDLIBSOPTIONS} -lpthread

${OBJECTDIR}/_ext/29dd86f/main.o: ../../main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../osef-spi/osef-posix/Signal -I../../../OLED -I../../../osef-spi/SPI -I../../../osef-spi/osef-posix/Mutex -I../../../osef-gpio/SysGPIO -I../../../osef-spi/osef-posix/Time -I../../../osef-spi/osef-posix/Socket -I../../../osef-spi/osef-posix/osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/main.o ../../main.cpp

# Subprojects
.build-subprojects:
	cd ../../../NetBeans/Pmod_OLED && ${MAKE}  -f Makefile CONF=Debug
	cd ../../../osef-spi/NetBeans/OSEF_SPI && ${MAKE}  -f Makefile CONF=Debug
	cd ../../../osef-gpio/NetBeans/OSEF_GPIO && ${MAKE}  -f Makefile CONF=Debug
	cd ../../../osef-spi/osef-posix/NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../../../NetBeans/Pmod_OLED && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../../../osef-spi/NetBeans/OSEF_SPI && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../../../osef-gpio/NetBeans/OSEF_GPIO && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../../../osef-spi/osef-posix/NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
