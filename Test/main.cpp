#include "SignalHandler.h"
#include "PmodOLED.h"
#include "TimeOut.h"
#include "SocketToolbox.h"
#include "Debug.h"

#include <cstdlib>
#include <iostream>
#include <unistd.h> // sleep
#include <string.h> // memset

void updateXYOn(uint8_t& x, int8_t& xa, uint8_t& y, int8_t& ya, bool& on)
{
    if((x==126)&&(y==30)&&(xa==1)&&(ya==1))
    {
        on = !on;
    }

    if((x==1)&&(y==1)&&(xa==-1)&&(ya==-1))
    {
        on = !on;
    }
    
    if(x==127)
    {
        x = 126;
        xa = -1;   
    }else if(x==0)
    {
        x = 1;
        xa = 1;
    }
    else
    {    
        x+=xa;
    }

    if(y==31)
    {
        y = 30;
        ya = -1;   
    } else if(y==0)
    {
        y = 1;
        ya = 1;
    }
    else
    {
        y+=ya;
    }
}

bool pixelTest( PMOD::PmodOLED& oled, bool& uturn1
                , uint8_t& x0, int8_t& x0a, uint8_t& y0, int8_t& y0a
                , uint8_t& x1, int8_t& x1a, uint8_t& y1, int8_t& y1a
                , bool& on)
{
    bool ret = true;
    
    if(oled.setPixel(x0, y0, on))
    {
        ret &= oled.writeBuffer();
    }
    updateXYOn(x0, x0a, y0, y0a, on);
    
    if((x0a==-1)&&(y0a==-1)&&!uturn1)
    {
        uturn1 = true;
    }
           
    if(uturn1)
    {
        if(oled.setPixel(x1, y1, !on))
        {
            ret &= oled.writeBuffer();
        }
        updateXYOn(x1, x1a, y1, y1a, on);
    }
    
    return ret;
}

bool verticalTest( PMOD::PmodOLED& oled, uint8_t& x, uint8_t& y, int8_t& ya, bool& on)
{
    bool ret = true;

    if(oled.drawVertical(x, 0, y, !on))
    {
        if(oled.drawVertical(x, y, 31-y, on))
        {
            if(oled.drawVertical(x, 31-y, 31, !on))
            {
                ret = oled.writeBuffer();

                if(y==15)
                {
                    y = 14;
                    ya = -1;   
                } else if(y==1)
                {
                    y = 2;
                    ya = 1;
                }
                else
                {
                    y+=ya;
                }

                if(x<127)
                {
                    x++;
                }
                else
                {
                    x = 0;
                    on = !on;
                }
            }
        }
    }
    
    return ret;
}

bool horizontalTest( PMOD::PmodOLED& oled, uint8_t& y, uint8_t& x, int8_t& xa, bool& on)
{
    bool ret = true;

    ret &= oled.drawHorizontal(y, 0, x, !on);
    ret &= oled.drawHorizontal(y, x, 127, !on);

    ret &= oled.drawHorizontal(y, x, 127-x, on);

    ret &= oled.writeBuffer();

    if(x==63)
    {
        x = 62;
        xa = -1;   
    } else if(x==1)
    {
        x = 2;
        xa = 1;
    }
    else
    {
        x+=xa;
    }
    
    if(y<31)
    {
        y++;
    }
    else
    {
        y = 0;
//        x = 0;
        on = !on;
    }
    
    return ret;
}

bool rectangleTest( PMOD::PmodOLED& oled, uint8_t& y, bool& on)
{
    bool ret = false;
    
    uint8_t x = y;
    if(oled.drawRectangle(x, 127-x, y, 31-y, on))
    {
        ret = oled.writeBuffer();
    }

    if(y<15)
    {
        y++;
    }
    else
    {
        y = 0;
        on = !on;
    }
    
    return ret;
}

bool characterTest( PMOD::PmodOLED& oled, size_t& x, uint8_t& c)
{
    bool ret = false;
    
//    if(x==0)
//    {
//        if(oled.drawHeart(x))
//        {
//            ret = oled.writeBuffer();
//            x+=4;
//        }
//    }
//    else
    {
        if(oled.drawCharacter7x7(c, x))
        {
            ret = oled.writeBuffer();
        }
    }

    x+=8;
    
    if(x>505)
    {
        x = 0;
    }
    
    c++;

    if((c>'9')&&(c<'A'))
    {
        c='A';
    }
    
    if(c>'Z')
    {
        c=' ';
    }
    
    sleep(1);
    
    return ret;
}

bool stringTest( PMOD::PmodOLED& oled, const uint8_t& x, const std::string& s)
{
    bool ret = true;
    
    if(oled.drawString3x5(s, x))
    {
        ret = oled.writeBuffer();
    }
    
    return ret;    
}

#include <ifaddrs.h> // getifaddrs
#include <netdb.h> // NI_MAXHOST
#include <vector>

typedef struct
{
    std::string interface;
    std::string address;
}IPL_Element;

typedef	std::vector<IPL_Element>    IPL_Container;

bool getIPList(IPL_Container& list)
{
    bool ret = false;
    
    struct ifaddrs *ifaddr = nullptr;
    if(getifaddrs(&ifaddr) != -1)
    {
        ret = true;
        
        struct ifaddrs *ifa = nullptr;
        char address[NI_MAXHOST];
        for(ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
        {
            if(ifa->ifa_addr!=NULL)
            {
                if(ifa->ifa_addr->sa_family == AF_INET)
                {
                    int s = getnameinfo(    ifa->ifa_addr,
                                        sizeof(struct sockaddr_in),
                                        address, NI_MAXHOST,
                                        NULL, 0, NI_NUMERICHOST);
                    
                    if(s!=0)
                    {
                        ret = false;
                        _DERR("getnameinfo() failed: "<<gai_strerror(s));
                    }
                    else
                    {
                        list.push_back({ifa->ifa_name,address});
                        _DOUT("interface "<<ifa->ifa_name<<" IPv4 address "<<address);
                    }
                }
            }
        }

        freeifaddrs(ifaddr);
    }
    
    return ret;
}

bool getIPv4(const std::string& interface, std::string& ip)
{
    bool ret = false;
    
    OSEF::InterfaceName ifName;
    OSEF::SocketToolbox::copyInterfaceName(ifName, interface);
    OSEF::IPv4AddressString ipv4 = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    if(OSEF::SocketToolbox::getIPv4(ifName, ipv4))
    {
        ip = ipv4;
        ret = true;
    }
    
    return ret;
}

bool ipAddressTest(PMOD::PmodOLED& oled)
{
    bool ret = false;
    
    IPL_Container iplist;
    if(getIPList(iplist))
    {
        _DOUT("IP list size "<<iplist.size());
        for(size_t i=0; i<iplist.size(); i++)
        {
            _DOUT("if "<<iplist[i].interface<<" ip "<<iplist[i].address);
            
            oled.drawString7x7("                ", 0);
            oled.drawString7x7(iplist[i].interface, 0);
            
            oled.drawString7x7("                ", 128);
            oled.drawString7x7(iplist[i].address, 136);
            
            oled.writeBuffer();
            
            sleep(3);
        }
        
        ret = true;
        
//        std::string ip = "";
//        if(getIPv4(interface, ip))
//        {
//            oled.drawString7x7(interface, 0);
//            oled.drawString7x7(ip, 136);
//
//            if(getIPv4("lo", ip))
//            {
//                oled.drawString7x7("lo", 256);
//                oled.drawString7x7(ip, 392);
//                oled.writeBuffer();
//
//                ret = true;
//            }
//
//            sleep(1);
//        }
    }
    
    return ret;
}

int main(int argc, char** argv)
{
    int ret = -1;
    
    OSEF::SignalHandler signal(SIGINT, SIGTERM);
    
    uint32_t periph = 0;
    uint32_t chip = 0;
    uint32_t dc = 19;
    uint32_t vdd = 18;
    uint32_t vbat = 20;
    uint32_t rst = 21;

    if(PMOD::PmodOLED::getPmodOledMapping(argc, argv, periph, chip, dc, vdd, vbat, rst))
    {
        PMOD::PmodOLED oled(periph, chip, 8000000/*8 MHz*/, dc, vdd, vbat, rst);
//
//        uint8_t c = '0';
//        size_t x = 0;
        
//        uint8_t x0 = 0;
//        uint8_t y0 = 0;        
//        int8_t x0a = 1;
//        int8_t y0a = 1;        
//        
//        uint8_t x1 = 0;
//        uint8_t y1 = 0;        
//        int8_t x1a = 1;
//        int8_t y1a = 1;
//        
//        bool on = true;
//        bool uturn1 = false;
        
//        uint16_t s = 0;
        
//        uint8_t buffer[512];
//        memset(&buffer[0], 0x00, 512);
//        uint8_t mask = 0x01;

        do
        {
            ret = -1;
            
//            memset(&buffer[0], mask, 512);
//            if(oled.writeBuffer(buffer))
//            {
//                mask<<=1;
//                if(mask==0)
//                {
//                    mask = 1;
//                }
////                std::cout<<"mask 0x"<<std::hex<<(uint32_t)(mask)<<std::dec<<std::endl;
////                sleep(1);
//                ret = 0;
//            }

            
//            if(oled.setSegment(s, 0xff))
//            {
//                if(oled.writeBuffer())
//                {
//                    std::cout<<"Segment "<<(uint32_t)(s)<<" ON"<<std::endl;
////                    sleep(1);
//                    if(oled.setSegment(s, 0x00))
//                    {
//                        if(oled.writeBuffer())
//                        {
//                            std::cout<<"Segment "<<(uint32_t)(s)<<" OFF"<<std::endl;
//                            
//                            s++;
//                            if(s>=512)
//                            {
//                                s = 0;
//                            }
//                            
//                            ret = 0;
//                        }
//                    }
//                }
//            }

            if(ipAddressTest(oled))
//            if(stringTest(oled, 0, "192.168.1.23 LOVE pierre LOVE"))
//            if(characterTest(oled, x, c))
//            if(rectangleTest(oled, y0, on))
//            if(horizontalTest(oled, y0, x0, x0a, on))
//            if(verticalTest(oled, x0, y0, y0a, on))
//            if(pixelTest( oled, uturn1, x0, x0a, y0, y0a, x1, x1a, y1, y1a, on))
            {
                ret = 0;
            }
            else
            {
                ret = -1;
            }
                    
//            OSEF::sleepms(1);
//            sleep(1);
        }while(oled.isPeripheralOK() && !signal.signalReceived() && (ret==0));
    }
    
    return ret;
}

