#ifndef PMODOLED_H
#define PMODOLED_H

#include "SpiDevice.h"
#include "SysOutputGPIO.h"

// 128x32 display

namespace PMOD {
    
    typedef uint8_t Char7x7Seg[7];
    
    typedef struct
    {
        uint8_t s0;
        uint8_t s1;
        uint8_t s2;
    }Char3x5Seg;
}

namespace PMOD { class PmodOLED {
public:
    PmodOLED(   const uint32_t& periph, const uint32_t& cs, const uint32_t& s
                ,const uint32_t& dc, const uint32_t& vdd, const uint32_t& vbat, const uint32_t& rst);
    PmodOLED();
    virtual ~PmodOLED();

    bool isPeripheralOK() const {return device.isPeripheralOK();}
    
    bool drawHeart(const uint8_t& x);
    
    bool drawString7x7(const std::string& s, const size_t& x);
    bool drawCharacter7x7(const uint8_t& c, const size_t& x);
    bool drawCharacterSegments(const Char7x7Seg& s, const size_t& x);
    
    bool drawString3x5(const std::string& s, const size_t& x);
    bool drawCharacter3x5(const uint8_t& c, const size_t& x);
    bool drawCharacterSegments(const Char3x5Seg& s, const size_t& x);
    
    bool drawRectangle(const uint8_t& x0, const uint8_t& x1, const uint8_t& y0, const uint8_t& y1, const bool& on);
    
    bool drawHorizontal(const uint8_t& y, const uint8_t& x0, const uint8_t& x1, const bool& on);
    
    bool drawVerticalOn(const uint8_t& x, const uint8_t& y0, const uint8_t& y1);
    bool drawVerticalOff(const uint8_t& x, const uint8_t& y0, const uint8_t& y1);
    bool drawVertical(const uint8_t& x, const uint8_t& y0, const uint8_t& y1, const bool& on);
    
    bool setPixelOn(const uint8_t& x, const uint8_t& y);
    bool setPixelOff(const uint8_t& x, const uint8_t& y);
    bool setPixel(const uint8_t& x, const uint8_t& y, const bool& on);
    
    bool setSegment(const uint16_t& n, const uint8_t& s);
    
    bool writeBuffer();
    
    bool writeBuffer(const uint8_t buff[512]);
    bool writeBuffer(uint8_t* buff, const size_t& n);

    static bool getPmodOledMapping(
        int argc, char** argv, uint32_t& periph
        ,uint32_t& cs,uint32_t& dc, uint32_t& vdd, uint32_t& vbat, uint32_t& rst);

//private:
//    PmodOLED(const PmodOLED& orig);
//    PmodOLED& operator=(const PmodOLED& orig);

    OSEF::SpiDevice device;
    OSEF::SysOutputGPIO DC; // Data/Command pin J1/7 (JA->GPIO19)
    OSEF::SysOutputGPIO Vdd; //  Logic voltage control pin J1/10 (JA->GPIO18)
    OSEF::SysOutputGPIO Vbat; // Battery voltage control pin J1/9 (JA->GPIO20)
    OSEF::SysOutputGPIO Reset; // reset pin J1/8 (JA->GPIO21)
    
    bool powered;
    bool configured;
    
    uint8_t buffer[512];
    
    static const Char7x7Seg specials7x7[16];
    static const Char7x7Seg capitals7x7[26];
    static const Char7x7Seg digits7x7[10];
    
    static const Char3x5Seg specials3x5[16];
    static const Char3x5Seg capitals3x5[26];
    static const Char3x5Seg digits3x5[10];
    
    bool setMaskHigherBits(uint8_t& mask);
    bool setMaskLowerBits(uint8_t& mask);
    bool getBufferSegmentMask(const uint8_t& x, const uint8_t& y, size_t& seg, uint8_t& mask);

    bool powerDown(); ///< power ON sequence including display configuration and power supplies
    bool powerUp(); ///< power OFF sequence including display and power supplies
    
    bool configureDisplay(/*struct fbtft_par *par*/); ///< display configuration commands sequence
    
    bool setVcomhLevel(const uint8_t& level); ///< set Vcomh deselect level [0-7]
    bool displayRAMPixels(); ///< display pixels according to RAM content
    bool displayONPixels(); ///< force display ON pixels only regardless of RAM content
    bool setNormalDisplayMode(); ///< send command to set normal display (RAM1=pixelON)
    bool setReverseDisplayMode(); ///< send command to set reverse display (RAM0=pixelON)
    bool deactivateScroll(); ///< send command to deactivate scroll
    bool displayOn(); ///< send dislay ON command
    bool displayOff(); ///< send display OFF command
    
    bool writeCommand(const uint8_t& cmd); ///< write command id, DC pin low
    bool writeCommand(const uint8_t& cmd, const uint8_t& val); ///< write command id and value, DC pin low
    bool writeCommand(const uint8_t& cmd, const uint8_t& val1, const uint8_t& val2); ///< write command id and values, DC pin low
    
    bool writeCommands(uint8_t* cmd, const size_t& n); ///< write n commands
    
    bool writeData(const uint8_t buff[512]); ///< write 512 display bytes and 512 stuffing bytes ... , DC pin high, 512 bytes for 128*32 pixels
    
    bool writeData(uint8_t* buff, const size_t& n); ///< write n display bytes, DC pin high

    bool reset(); ///< set reset pin low for 10 ms
    
    bool logicOn(); ///< set Vdd pin high
    bool logicOff(); ///< set Vdd pin low
    
    bool batteryOn(); ///< set Vbat pin high
    bool batteryOff(); ///< set Vbat pin low
    
private:
    PmodOLED(const PmodOLED& orig);
    PmodOLED& operator=(const PmodOLED& orig);
    
};}

#endif /* PMODOLED_H */
