#include "PmodOLED.h"
#include "TimeOut.h"
#include "Debug.h"

#include "getopt.h" // getopt

#define SSD1306_DISPLAYON           0xAF ///< See datasheet
#define SSD1306_SETDISPLAYCLOCKDIV  0xD5 ///< See datasheet
#define SSD1306_SETMULTIPLEX        0xA8 ///< See datasheet
#define SSD1306_SETDISPLAYOFFSET    0xD3 ///< See datasheet
#define SSD1306_SETSTARTLINE        0x40 ///< See datasheet
#define SSD1306_SETCONTRAST         0x81 ///< See datasheet
#define SSD1306_CHARGEPUMP          0x8D ///< See datasheet
#define SSD1306_SEGREMAP            0xA0 ///< See datasheet
#define SSD1306_COMSCANDEC          0xC8 ///< See datasheet
#define SSD1306_SETCOMPINS          0xDA ///< See datasheet
#define SSD1306_SETPRECHARGE        0xD9 ///< See datasheet
#define SSD1306_MEMORYMODE          0x20 ///< See datasheet
#define SSD1306_COLUMNADDR          0x21 ///< See datasheet
#define SSD1306_PAGEADDR            0x22 ///< See datasheet
#define SSD1306_DISPLAYALLON_RESUME 0xA4 ///< See datasheet
#define SSD1306_DISPLAYALLON        0xA5 ///< See datasheet
#define SSD1306_NORMALDISPLAY       0xA6 ///< See datasheet
#define SSD1306_INVERTDISPLAY       0xA7 ///< See datasheet
#define SSD1306_DEACTIVATE_SCROLL   0x2E ///< Stop scroll
#define SSD1306_DISPLAYOFF          0xAE ///< See datasheet

const PMOD::Char7x7Seg PMOD::PmodOLED::specials7x7[16] = {
    {0x0,0x0,0x0,0x0,0x0,0x0,0x0}
    ,{0x0,0x0,0x0,0xBE,0x0,0x0,0x0}
    ,{0x0,0x0,0x6,0x0,0x6,0x0,0x0}
    ,{0x0,0x28,0x7C,0x28,0x7C,0x28,0x0}
    ,{0x48,0x54,0xFE,0x54,0xFE,0x54,0x24}
    ,{0x84,0x4A,0x24,0x10,0x48,0xA4,0x42}
    ,{0x6C,0x92,0x92,0x92,0xAC,0x40,0xA0}
    ,{0x0,0x0,0x6,0x0,0x0,0x0,0x0}
    ,{0x7C,0x82,0x0,0x0,0x0,0x0,0x0}
    ,{0x0,0x0,0x0,0x0,0x0,0x82,0x7C}
    ,{0x0,0x28,0x10,0x7C,0x10,0x28,0x0}
    ,{0x0,0x10,0x10,0x7C,0x10,0x10,0x0}
    ,{0x0,0x0,0x80,0x40,0x0,0x0,0x0}
    ,{0x0,0x10,0x10,0x10,0x10,0x10,0x0}
    ,{0x0,0x0,0x0,0x80,0x0,0x0,0x0}
    ,{0x80,0x40,0x20,0x10,0x8,0x4,0x2}
};

const PMOD::Char7x7Seg PMOD::PmodOLED::capitals7x7[26] = {
    {0xFC,0x12,0x12,0x12,0x12,0x12,0xFC}
    ,{0xFE,0x92,0x92,0x92,0x92,0x92,0x6C}
    ,{0x7C,0x82,0x82,0x82,0x82,0x82,0x82}
    ,{0xFE,0x82,0x82,0x82,0x82,0x82,0x7C}
    ,{0xFE,0x92,0x92,0x92,0x82,0x82,0x82}
    ,{0xFE,0x12,0x12,0x12,0x2,0x2,0x2}
    ,{0x7C,0x82,0x82,0x82,0x92,0x92,0xF2}
    ,{0xFE,0x10,0x10,0x10,0x10,0x10,0xFE}
    ,{0x82,0x82,0x82,0xFE,0x82,0x82,0x82}
    ,{0x42,0x82,0x82,0x82,0x82,0x82,0x7E}
    ,{0xFE,0x10,0x10,0x10,0x28,0x44,0x82}
    ,{0xFE,0x80,0x80,0x80,0x80,0x80,0x80}
    ,{0xFE,0x4,0x8,0x10,0x8,0x4,0xFE}
    ,{0xFE,0x4,0x8,0x10,0x20,0x40,0xFE}
    ,{0x7C,0x82,0x82,0x82,0x82,0x82,0x7C}
    ,{0xFE,0x12,0x12,0x12,0x12,0x12,0xC}
    ,{0x7C,0x82,0x82,0x82,0xA2,0x42,0xBC}
    ,{0xFE,0x12,0x12,0x12,0x32,0x52,0x8C}
    ,{0x8C,0x92,0x92,0x92,0x92,0x92,0x62}
    ,{0x2,0x2,0x2,0xFE,0x2,0x2,0x2}
    ,{0x7E,0x80,0x80,0x80,0x80,0x80,0x7E}
    ,{0x1E,0x20,0x40,0x80,0x40,0x20,0x1E}
    ,{0x7E,0x80,0x80,0x70,0x80,0x80,0x7E}
    ,{0x82,0x44,0x28,0x10,0x28,0x44,0x82}
    ,{0x2,0x4,0x8,0xF0,0x8,0x4,0x2}
    ,{0x82,0xC2,0xA2,0x92,0x8A,0x86,0x82}
};

const PMOD::Char7x7Seg PMOD::PmodOLED::digits7x7[10] = {
    {0x7C,0xC2,0xA2,0x92,0x8A,0x86,0x7C}
    ,{0x0,0x0,0x0,0xFE,0x0,0x0,0x0}
    ,{0xE2,0x92,0x92,0x92,0x92,0x92,0x8C}
    ,{0x82,0x82,0x82,0x92,0x92,0x92,0x6C}
    ,{0xE,0x10,0x10,0x10,0x10,0x10,0xFE}
    ,{0x9E,0x92,0x92,0x92,0x92,0x92,0x62}
    ,{0x7C,0x92,0x92,0x92,0x92,0x92,0x62}
    ,{0x2,0x82,0x42,0x22,0x12,0xA,0x6}
    ,{0x6C,0x92,0x92,0x92,0x92,0x92,0x6C}
    ,{0x8C,0x92,0x92,0x92,0x92,0x92,0x7C}
};

const PMOD::Char3x5Seg PMOD::PmodOLED::specials3x5[16] = {
    {0x00, 0x00, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x04, 0x00}
    ,{0x00, 0x10, 0x00}
    ,{0x00, 0x04, 0x00}
};

const PMOD::Char3x5Seg PMOD::PmodOLED::capitals3x5[26] = {
    {0x1e, 0x05, 0x1e}  // A
    ,{0x1f, 0x15, 0x0a} // B
    ,{0x0e, 0x11, 0x11} // C
    ,{0x1f, 0x11, 0x0e} // D
    ,{0x1f, 0x15, 0x11} // E
    ,{0x1f, 0x05, 0x01} // F
    ,{0x0e, 0x11, 0x0d} // G
    ,{0x1f, 0x04, 0x1f} // H
    ,{0x11, 0x1f, 0x11} // I
    ,{0x11, 0x1f, 0x01} // J
    ,{0x1f, 0x04, 0x1b} // K
    ,{0x1f, 0x10, 0x10} // L
    ,{0x1f, 0x02, 0x1f} // M
    ,{0x1f, 0x0e, 0x1f} // N
    ,{0x0e, 0x11, 0x0e} // O
    ,{0x1f, 0x05, 0x02} // P
    ,{0x02, 0x05, 0x1f} // Q
    ,{0x1f, 0x05, 0x1a} // R
    ,{0x12, 0x15, 0x09} // S
    ,{0x01, 0x1f, 0x01} // T
    ,{0x1f, 0x10, 0x1f} // U
    ,{0x0f, 0x10, 0x0f} // V
    ,{0x1f, 0x08, 0x1f} // W
    ,{0x1b, 0x04, 0x1b} // X
    ,{0x03, 0x1c, 0x03} // Y
    ,{0x19, 0x15, 0x13} // Z
};

const PMOD::Char3x5Seg PMOD::PmodOLED::digits3x5[10] = {
    {0x0e, 0x11, 0x0e}  // 0
    ,{0x12, 0x1f, 0x10} // 1
    ,{0x19, 0x15, 0x12} // 2
    ,{0x11, 0x15, 0x0a} // 3
    ,{0x03, 0x04, 0x1f} // 4
    ,{0x13, 0x15, 0x09} // 5
    ,{0x0e, 0x15, 0x09} // 6
    ,{0x01, 0x19, 0x07} // 7
    ,{0x0a, 0x15, 0x0a} // 8
    ,{0x02, 0x05, 0x1e} // 9
};

PMOD::PmodOLED::PmodOLED(   const uint32_t& periph, const uint32_t& cs, const uint32_t& s
                            ,const uint32_t& dc, const uint32_t& vdd, const uint32_t& vbat, const uint32_t& rst)
    :device(periph, cs, 0, s)
    ,DC(dc)
    ,Vdd(vdd)
    ,Vbat(vbat)
    ,Reset(rst)
    ,powered(false)
    ,configured(false)
    ,buffer()
{
    memset(&buffer[0], 0x00, 512);
}

PMOD::PmodOLED::PmodOLED()
    :device(0, 0, 0, 8000000)
    ,DC(19)
    ,Vdd(18)
    ,Vbat(20)
    ,Reset(21)
    ,powered(false)
    ,configured(false){}

PMOD::PmodOLED::~PmodOLED()
{
    powerDown();
}

// from Univision UG-2832HSWEG04 datasheet
// 4.2.1  Power up Sequence:
//  1.   Power up VDD
//  2.   Send Display off command
//  3.   Initialization
//  4.   Clear   Screen
//  5.   Power up VCC
//  6.   Delay   100ms   (When VCC is stable)
//  7.   Send Display on command
bool PMOD::PmodOLED::powerUp()
{
    bool ret = false;

    if(!powered)
    {
        if(reset()) // Additional reset sequence before one-shot power-up sequence
        {
            if(logicOn())                   // set Vdd pin high
            {
                if(displayOff())            // send display OFF command (0xAE)
                {
                    if(configureDisplay())  // send various configuration commands
                    {
                        uint8_t clearBuffer[512];
                        memset(&clearBuffer[0], 0x00, 512);
                        if(writeData(clearBuffer))      // send cleared data buffer to clear screen
                        {
                            if(batteryOn())             // set Vbat pin high
                            {
                                if(OSEF::sleepms(100))  // delay 100ms
                                {
                                    if(displayOn())     // send Display ON command (SSD1306_DISPLAYON)
                                    {
                                        ret = true;
                                        powered = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

// from Univision UG-2832HSWEG04 datasheet
// 4.2.2  Power down Sequence:
//  1.   Send Display off command
//  2.   Power down VCC
//  3.   Delay   100ms   (When  VCC  is  reach  0  and  panel  is completely discharges)
//  4.   Power down VDD
bool PMOD::PmodOLED::powerDown()
{
    bool ret = true;

    if(powered)
    {
        ret &= displayOff();        // send display OFF command (0xAE)
        ret &= batteryOff();        // set Vbat pin low
        ret &= OSEF::sleepms(100);  // delay 100 ms
        ret &= logicOff();          // set Vdd pin low

        if(ret)
        {
            powered = false;
        }
    }

    return ret;
}

// from Solomon Systech SSD1306 datasheet
//    Set MUX Ratio A8h, 3Fh
//    Set Display Offset D3h, 00h
//    Set Display Start Line 40h
//    Set Segment  re-map A0h/A1h
//    Set COM Output Scan Direction C0h/C8h
//    Set COM Pins hardware configuration DAh, 02
//    Set Contrast Control 81h, 7Fh
//    Disable Entire Display On A4h
//    Set Normal Display A6h
//    Set Osc Frequency  D5h, 80h
//    Enable charge pump regulator 8Dh, 14h
//    Display On AFh

// from Univision UG-2832HSWEG04 datasheet
// 4.4   Actual Application Example
//  Set Display Off 0xAE
//  Set Display Clock Divide Ratio/Oscillator Frequency 0xD5, 0x80
//  Set Multiplex Ratio 0xA8, 0x1F
//  Set Display Offset 0xD3, 0x00
//  Set Display Start Line 0x40
//  Set Charge Pump 0x8D,
//      0x10(VCC Supplied Externally) /
//      0x14(VCC Generated by Internal DC/DC Circuit)
//  Set Segment Re-Map 0xA1
//  Set COM Output Scan Direction 0xC8
//  Set COM Pins Hardware Configuration 0xDA, 0x02
//  Set Contrast Control 0x81, 0x8F
//  Set Pre-Charge Period 0xD9,
//      0x22(VCC Supplied Externally) /
//      0xF1(VCC Generated by Internal DC/DC Circuit)
//  Set VCOMH Deselect Level 0xDB, 0x40
//  Set Entire Display On/Off 0xA4
//  Set Normal/Inverse Display 0xA6
//  Clear Screen
//  Set Display On 0xAF

// from Adafruit SS1306 library
//  SSD1306_DISPLAYOFF              0xAE
//  SSD1306_SETDISPLAYCLOCKDIV      0xD5, 0x80
//  SSD1306_SETMULTIPLEX            0xA8, (HEIGHT - 1)
//  SSD1306_SETDISPLAYOFFSET        0xD3, 0x00
//  SSD1306_SETSTARTLINE | 0x0      0x40 | 0x00
//  SSD1306_CHARGEPUMP              0x8D
//  SSD1306_EXTERNALVCC             (vccstate == SSD1306_EXTERNALVCC) ? 0x10 : 0x14
//  SSD1306_MEMORYMODE              0x20, 0x00 (act like ks0108)
//  SSD1306_SEGREMAP | 0x1          0xA0 | 0x01
//  SSD1306_COMSCANDEC              0xC8
//  SSD1306_SETCOMPINS              0xDA, 0x02
//  SSD1306_SETCONTRAST             0x81, 0x8F
//  SSD1306_SETPRECHARGE            0xD9, ((vccstate == SSD1306_EXTERNALVCC) ? 0x22 : 0xF1)
//  SSD1306_SETVCOMDETECT           0xDB, 0x40
//  SSD1306_DISPLAYALLON_RESUME     0xA4
//  SSD1306_NORMALDISPLAY           0xA6
//  SSD1306_DEACTIVATE_SCROLL       0x2E
//  SSD1306_DISPLAYON               0xAF
bool PMOD::PmodOLED::configureDisplay()
{
    bool ret = true;

    if(!configured)
    {
        //Initialize display to desired operating mode

        // Set display clock divide ratio (bits 0-3) and oscillator frequency (bits 4-7) to default 0x80
        ret &= writeCommand(SSD1306_SETDISPLAYCLOCKDIV, 0x80); // 0xD5 // UG-2832 0x80 // SSD1306 0x80 // Adafruit 0x80

        // Set multiplex ratio to (height-1) !?
        ret &= writeCommand(SSD1306_SETMULTIPLEX, 0x1F); // 0xA8 // UG-2832 0x1F // SSD1306 0x3F // Adafruit (HEIGHT - 1)

        // Set display offset (0=no offset) and start line
        ret &= writeCommand(SSD1306_SETDISPLAYOFFSET, 0x00); //0xD3 // UG-2832 0x00 // SSD1306 0x00 // Adafruit 0x00

        // Set display start line 0x40|[0-3F]
        ret &= writeCommand(SSD1306_SETSTARTLINE|0x00); // 0x40 // UG-2832 0x40 // SSD1306 0x40 // Adafruit 0x40

        // Set charge pump setting, 0x14=enable when display ON, 0x10=disable
        ret &= writeCommand(SSD1306_CHARGEPUMP, 0x14); // 0x8D // UG-2832 0x10 or 0x14 // SSD1306 0x14 // MPLAB 0x14 // Adafruit (vccstate == SSD1306_EXTERNALVCC) ? 0x10 : 0x14

        // Set segment re-map, 0xA0=column 0 to SEG0, 0xA1=column 127 to SEG0
        ret &= writeCommand(SSD1306_SEGREMAP|0x1); // UG-2832 0xA1 // SSD1306 0xA0 or 0xA1 // MPLAB 0xA1 // Adafruit 0xA1

        // Set COM output scan direction, 0xC0=from COM0 to COMn , 0xC8=from COMn to COM0
        ret &= writeCommand(SSD1306_COMSCANDEC); // UG-2832 0xC8 // SSD1306 0xC0 or 0xC8 // MPLAB 0xC8 // Adafruit 0xC8

        // !!!!
        // Set COM pins hardware remap A5|A4|0x02
        // A4=0b Sequential, A4=1b Alternative (default) COM pin configuration
        // A5=0b disable (default), A5=1b enable left/right remap
        ret &= writeCommand(SSD1306_SETCOMPINS, 0x02); // 0xDA // UG-2832 0x02 // SSD1306 0x02 // MPLAB 0x20 // Adafruit 0x02

        // Set contrast from 0x00 to 0xff
        ret &= writeCommand(SSD1306_SETCONTRAST, 0x8F); // 0x81 // UG-2832 0x8F // SSD1306 0x7F // Adafruit 0x8F

        // Set Pre-charge Period in number of DCLK where RESET=2xDCLK
        ret &= writeCommand(SSD1306_SETPRECHARGE, 0xF1); // 0xD9 // UG-2832 0x22 or 0xF1 // SSD1306 N/A // MPLAB 0xF1 // Adafruit ((vccstate == SSD1306_EXTERNALVCC) ? 0x22 : 0xF1)

        // Set VCOMH Deselect Level
        ret &= setVcomhLevel(4); // 0xDB // UG-2832 0x40 // SSD1306 N/A // Adafruit 0x40

         // Display RAM content
        ret &= displayRAMPixels(); // 0xA4 // UG-2832 0xA4 // SSD1306 0xA4 // Adafruit 0xA4

        // Set normal display
        ret &= setNormalDisplayMode(); // 0xA6 // UG-2832 0xA6 // SSD1306 0xA6 // Adafruit 0xA6

        // end of display datasheet configuration sequence

        // Set Memory Addressing Mode, 0x0=horizontal, 0x1=vertical, 0x2=page
        ret &= writeCommand(SSD1306_MEMORYMODE, 0x0); // 0x20 // UG-2832 N/A // SSD1306 N/A // Adafruit 0x00

        // Deactivate scroll
        ret &= deactivateScroll(); // 0x2E // UG-2832 N/A // SSD1306 N/A // Adafruit 0x2E

        configured = ret;
    }

    return ret;
}

bool PMOD::PmodOLED::setVcomhLevel(const uint8_t& level)
{
    bool ret = false;

    if(level<=7)
    {
        // 0x00=0.65Vcc, 0x20=0.77Vcc, 0x30=0.83Vcc
        ret = writeCommand(0xDB, level<<4);
        if(ret)
        {
            _DOUT("Display pixels according RAM");
        }
        else
        {
            _DERR("Failed to display pixels according RAM");
        }
    }
    else
    {
        _DERR("Wrong Vcomh level "<<level<<" must be in [0-7]");
    }

    return ret;
}

bool PMOD::PmodOLED::displayRAMPixels()
{
    const bool ret = writeCommand(SSD1306_DISPLAYALLON_RESUME);

    if(ret)
    {
        _DOUT("Display pixels according RAM");
    }
    else
    {
        _DERR("Failed to display pixels according RAM");
    }

    return ret;
}

bool PMOD::PmodOLED::displayONPixels()
{
    const bool ret = writeCommand(SSD1306_DISPLAYALLON);

    if(ret)
    {
        _DOUT("Display ON pixels only");
    }
    else
    {
        _DERR("Failed to display ON pixels only");
    }

    return ret;
}

bool PMOD::PmodOLED::setNormalDisplayMode()
{
    const bool ret = writeCommand(SSD1306_NORMALDISPLAY);

    if(ret)
    {
        _DOUT("Normal display mode");
    }
    else
    {
        _DERR("Failed to set normal display mode");
    }

    return ret;
}

bool PMOD::PmodOLED::setReverseDisplayMode()
{
    const bool ret = writeCommand(SSD1306_INVERTDISPLAY);

    if(ret)
    {
        _DOUT("Reverse display mode");
    }
    else
    {
        _DERR("Failed to set reverse display mode");
    }

    return ret;
}

bool PMOD::PmodOLED::deactivateScroll()
{
    const bool ret = writeCommand(SSD1306_DEACTIVATE_SCROLL);

    if(ret)
    {
        _DOUT("Scroll deactivated");
    }
    else
    {
        _DERR("Failed to deactivate scroll");
    }

    return ret;
}

bool PMOD::PmodOLED::displayOn()
{
    const bool ret = writeCommand(SSD1306_DISPLAYON);

    if(ret)
    {
        _DOUT("Display ON");
    }
    else
    {
        _DERR("Failed to set display ON");
    }

    return ret;
}

bool PMOD::PmodOLED::displayOff()
{
    const bool ret = writeCommand(SSD1306_DISPLAYOFF);

    if(ret)
    {
        _DOUT("Display OFF");
    }
    else
    {
        _DERR("Failed to set display OFF");
    }

    return ret;
}

bool PMOD::PmodOLED::writeCommand(const uint8_t& cmd)
{
    bool ret = false;

    if(DC.setLow())
    {
        if(device.writeByte(cmd))
        {
            ret = true;
        }
        else
        {
            _DERX("Failed to write command 0x"<<cmd);
        }
    }
    else
    {
        _DERR("Failed to set D/C Low");
    }

    return ret;
}

bool PMOD::PmodOLED::writeCommand(const uint8_t& cmd, const uint8_t& val)
{
    bool ret = false;

    if(DC.setLow())
    {
        if(device.writeByte(cmd))
        {
            if(device.writeByte(val))
            {
                ret = true;
            }
            else
            {
                _DERX("Failed to write value 0x"<<val<<" for command 0x"<<cmd);
            }
        }
        else
        {
            _DERX("Failed to write command 0x"<<cmd);
        }
    }
    else
    {
        _DERR("Failed to set D/C Low");
    }

    return ret;
}

bool PMOD::PmodOLED::writeCommand(const uint8_t& cmd, const uint8_t& val1, const uint8_t& val2)
{
    bool ret = false;

    if(DC.setLow())
    {
        if(device.writeByte(cmd))
        {
            if(device.writeByte(val1))
            {
                if(device.writeByte(val2))
                {
                    ret = true;
                }
                else
                {
                    _DERX("Failed to write value 0x"<<val2<<" for command 0x"<<cmd);
                }
            }
            else
            {
                _DERX("Failed to write value 0x"<<val1<<" for command 0x"<<cmd);
            }
        }
        else
        {
            _DERX("Failed to write command 0x"<<cmd);
        }
    }
    else
    {
        _DERR("Failed to set D/C Low");
    }

    return ret;
}

bool PMOD::PmodOLED::writeCommands(uint8_t* cmd, const size_t& n)
{
    bool ret = false;

    if(cmd!=nullptr)
    {
        if(DC.setLow())
        {
            size_t i=0;
            do
            {
                ret = device.writeByte(cmd[i]);
                if(ret)
                {
                    i++;
                }
                else
                {
                    _DERX("Failed to write command #"<<i<<" 0x"<<cmd[i]);
                }
            }while(ret && (i<n));
        }
        else
        {
            _DERR("Failed to set D/C Low");
        }
    }
    else
    {
        _DERR("commands pointer is null");
    }

    return ret;
}

bool PMOD::PmodOLED::drawHeart(const uint8_t& x)
{
    bool ret = false;

    if(x<=120)
    {
        buffer[x] = 0x00;
        buffer[x+1] = 0x06;
        buffer[x+2] = 0x0f;
        buffer[x+3] = 0x1e;
        buffer[x+4] = 0x0f;
        buffer[x+5] = 0x06;
        buffer[x+6] = 0x00;
        buffer[x+7] = 0x00;

        ret = true;
    }

    return ret;
}

bool PMOD::PmodOLED::drawString7x7(const std::string& s, const size_t& x)
{
    bool ret = false;

    size_t i = 0;
    do
    {
        ret = drawCharacter7x7(s[i], x+(i*8));
        i++;
    }while(ret && (i<s.size()));

    return ret;
}

bool PMOD::PmodOLED::drawString3x5(const std::string& s, const size_t& x)
{
    bool ret = false;

    size_t i = 0;
    do
    {
        ret = drawCharacter3x5(s[i], x+(i*4));
        i++;
    }while(ret && (i<s.size()));

    return ret;
}

bool PMOD::PmodOLED::drawCharacter7x7(const uint8_t& c, const size_t& x)
{
    bool ret = false;

    if((c>=' ')&&(c<='/'))
    {
        ret = drawCharacterSegments(specials7x7[c-' '], x);
    }
    else if((c>='0')&&(c<='9'))
    {
        ret = drawCharacterSegments(digits7x7[c-'0'], x);
    }
    else if((c>='A')&&(c<='Z'))
    {
        ret = drawCharacterSegments(capitals7x7[c-'A'], x);
    }
    else if((c>='a')&&(c<='z'))
    {
        ret = drawCharacterSegments(capitals7x7[c-'a'], x);
    }
    else
    {
        ret = false;
    }

    return ret;
}

bool PMOD::PmodOLED::drawCharacterSegments(const Char7x7Seg& s, const size_t& x)
{
    bool ret = false;

    if(x<=505)
    {
        memcpy(&buffer[x], &s[0], 7);
        ret = true;
    }

    return ret;
}

bool PMOD::PmodOLED::drawCharacter3x5(const uint8_t& c, const size_t& x)
{
    bool ret = false;

//    if(c==' ')
//    {
//        ret = drawCharacterSegments({0,0,0}, x);
//    }
//    else if(c=='.')
//    {
//        ret = drawCharacterSegments({0,0x10,0}, x);
//    }
    if((c>=' ')&&(c<='/'))
    {
        ret = drawCharacterSegments(specials3x5[c-' '], x);
    }
    else if((c>='0')&&(c<='9'))
    {
        ret = drawCharacterSegments(digits3x5[c-'0'], x);
    }
    else if((c>='A')&&(c<='Z'))
    {
        ret = drawCharacterSegments(capitals3x5[c-'A'], x);
    }
    else if((c>='a')&&(c<='z'))
    {
        ret = drawCharacterSegments(capitals3x5[c-'a'], x);
    }
    else
    {
        ret = false;
    }

    return ret;
}

bool PMOD::PmodOLED::drawCharacterSegments(const Char3x5Seg& s, const size_t& x)
{
    bool ret = false;

    if(x<=508)
    {
        buffer[x] = s.s0;
        buffer[x+1] = s.s1;
        buffer[x+2] = s.s2;
        buffer[x+3] = 0;

        ret = true;
    }

    return ret;
}

bool PMOD::PmodOLED::drawRectangle(const uint8_t& x0, const uint8_t& x1, const uint8_t& y0, const uint8_t& y1, const bool& on)
{
    bool ret = true;

    ret &= drawVertical(x0, y0, y1, on);
    ret &= drawVertical(x1, y0, y1, on);

    ret &= drawHorizontal(y0, x0, x1, on);
    ret &= drawHorizontal(y1, x0, x1, on);

    return ret;
}

bool PMOD::PmodOLED::drawVerticalOn(const uint8_t& x, const uint8_t& y0, const uint8_t& y1)
{
    const bool ret = drawVertical(x, y0, y1, true);
    return ret;
}

bool PMOD::PmodOLED::drawVerticalOff(const uint8_t& x, const uint8_t& y0, const uint8_t& y1)
{
    const bool ret = drawVertical(x, y0, y1, false);
    return ret;
}

bool PMOD::PmodOLED::drawVertical(const uint8_t& x, const uint8_t& y0, const uint8_t& y1, const bool& on)
{
    bool ret = false;

    if(y0<y1)
    {
        size_t seg0 = 0;
        uint8_t mask0 = 0;
        if(getBufferSegmentMask(x, y0, seg0, mask0))
        {
            size_t seg1 = 0;
            uint8_t mask1 = 0;
            if(getBufferSegmentMask(x, y1, seg1, mask1))
            {
                if(seg0<seg1)
                {
                    if(setMaskHigherBits(mask0))
                    {
                        if(setMaskLowerBits(mask1))
                        {
                            if(on)
                            {
                                buffer[seg0] = buffer[seg0] | mask0;
                                for(size_t i=seg0+128; i<seg1; i+=128)
                                {
                                    buffer[i] = 0xff;
                                }
                                buffer[seg1] = buffer[seg1] | mask1;
                            }
                            else
                            {
                                buffer[seg0] = buffer[seg0] & ~mask0;
                                for(size_t i=seg0+128; i<seg1; i+=128)
                                {
                                    buffer[i] = 0x00;
                                }
                                buffer[seg1] = buffer[seg1] & ~mask1;
                            }

                            ret = true;
                        }
                    }
                }
                else
                {
                    uint8_t mask = mask0;
                    uint8_t i = 1;
                    while((mask0<<i)<mask1)
                    {
                        mask |= mask0<<i;
                        i++;
                    }
                    mask |= mask1;

                    if(on)
                    {
                        buffer[seg0] = buffer[seg0] | mask;
                    }
                    else
                    {
                        buffer[seg0] = buffer[seg0] & ~mask;
                    }

                    ret = true;
                }
            }
        }
    }

    return ret;
}

bool PMOD::PmodOLED::drawHorizontal(const uint8_t& y, const uint8_t& x0, const uint8_t& x1, const bool& on)
{
    bool ret = false;

    if(x0<x1)
    {
        size_t seg0 = 0;
        uint8_t mask0 = 0;
        if(getBufferSegmentMask(x0, y, seg0, mask0))
        {
            size_t seg1 = 0;
            uint8_t mask1 = 0;
            if(getBufferSegmentMask(x1, y, seg1, mask1))
            {
                if(on)
                {
                    for(size_t i=seg0; i<seg1; i++)
                    {
                        buffer[i] |= mask0;
                    }
                }
                else
                {
                    for(size_t i=seg0; i<seg1; i++)
                    {
                        buffer[i] &= ~mask0;
                    }
                }

                ret = true;
            }
        }
    }

    return ret;
}

bool PMOD::PmodOLED::setPixelOn(const uint8_t& x, const uint8_t& y)
{
    const bool ret = setPixel(x, y, true);
    return ret;
}

bool PMOD::PmodOLED::setPixelOff(const uint8_t& x, const uint8_t& y)
{
    const bool ret = setPixel(x, y, false);
    return ret;
}

bool PMOD::PmodOLED::setPixel(const uint8_t& x, const uint8_t& y, const bool& on)
{
    bool ret = false;

    size_t seg = 0;
    uint8_t mask = 0;
    if(getBufferSegmentMask(x, y, seg, mask))
    {
        if(on)
        {
            buffer[seg] = buffer[seg] | mask;
        }
        else
        {
            buffer[seg] = buffer[seg] & ~mask;
        }

//        _DOUT("on="<<on<<" x="<<(uint32_t)(x)<<" y="<<(uint32_t)(y)<<" seg="<<seg<<" mask="<<(uint32_t)(mask)<<" buffer["<<seg<<"]="<<(uint32_t)(buffer[seg]));

        ret = true;
    }

    return ret;
}

bool PMOD::PmodOLED::setSegment(const uint16_t& n, const uint8_t& s)
{
    bool ret = false;

    if(n<512)
    {
        buffer[n] = s;
        ret = true;
    }

    return ret;
}

bool PMOD::PmodOLED::setMaskHigherBits(uint8_t& mask)
{
    bool ret = true;

    switch(mask)
    {
        case 0x01:
            mask = 0xff;
            break;
        case 0x02:
            mask = 0xfe;
            break;
        case 0x04:
            mask = 0xfc;
            break;
        case 0x08:
            mask = 0xf8;
            break;
        case 0x10:
            mask = 0xf0;
            break;
        case 0x20:
            mask = 0xe0;
            break;
        case 0x40:
            mask = 0xc0;
            break;
        case 0x80:
            break;
        default:
            ret = false;
            break;
    }

    return ret;
}

bool PMOD::PmodOLED::setMaskLowerBits(uint8_t& mask)
{
    bool ret = true;

    switch(mask)
    {
        case 0x01:
            break;
        case 0x02:
            mask = 0x03;
            break;
        case 0x04:
            mask = 0x07;
            break;
        case 0x08:
            mask = 0x0f;
            break;
        case 0x10:
            mask = 0x1f;
            break;
        case 0x20:
            mask = 0x3f;
            break;
        case 0x40:
            mask = 0x7f;
            break;
        case 0x80:
            mask = 0xff;
            break;
        default:
            ret = false;
            break;
    }

    return ret;
}

bool PMOD::PmodOLED::getBufferSegmentMask(const uint8_t& x, const uint8_t& y, size_t& seg, uint8_t& mask)
{
    bool ret = false;

    if((x<128) && (y<32))
    {
        seg = x + (y/8)*128;
        mask = 0x01<<(y%8);
        ret = true;
    }

    return ret;
}

bool PMOD::PmodOLED::writeBuffer()
{
    const bool ret = writeBuffer(buffer);
    return ret;
}

bool PMOD::PmodOLED::writeBuffer(const uint8_t buff[512])
{
    bool ret = false;

    if(powerUp())
    {
        ret = writeData(buff);
    }

    return ret;
}

bool PMOD::PmodOLED::writeBuffer(uint8_t* buff, const size_t& n)
{
    bool ret = false;

    if(powerUp())
    {
        ret = writeData(buff, n);
    }

    return ret;
}

bool PMOD::PmodOLED::writeData(const uint8_t buff[512])
{
    bool ret = false;

    if(writeCommand(SSD1306_PAGEADDR, 0x00, 0xFF)) // 0x22, Page start address, Page end (not really, but works here)
    {
        if(writeCommand(SSD1306_COLUMNADDR, 0x00, 0x7F)) // 0x21, Column start address, (WIDTH - 1)
        {
            if(DC.setHigh())
            {
                size_t i = 0;
                do
                {
                    if(device.writeByte(buff[i]))
                    {
                        i++;
                        ret = true;
                    }
                    else
                    {
                        _DERR("Failed to write buffer byte "<<i);
                    }
                }while((i<512) && ret);
            }
            else
            {
                _DERR("Failed to set D/C High");
            }
        }
    }
    return ret;
}

bool PMOD::PmodOLED::writeData(uint8_t* buff, const size_t& n)
{
    bool ret = false;

    if(DC.setHigh())
    {
        size_t i = 0;
        do
        {
            if(device.writeByte(buff[i]))
            {
                i++;
                ret = true;
            }
            else
            {
                _DERR("Failed to write buffer byte "<<i);
            }
        }while((i<n) && ret);
    }
    else
    {
        _DERR("Failed to set D/C High");
    }

    return ret;
}

// from Univision UG-2832HSWEG04 datasheet
// 4.3   Reset   Circuit
//  When RES# input is low, the chip is initialized with the following status:
//  1.   Display is OFF
//  2.   128×64 Display Mode
//  3.   Normal  segment  and  display  data  column  and  row  address  mapping
//        (SEG0  mapped to column address 00h and COM0 mapped to row address 00h)
//  4.   Shift register data clear in serial interface
//  5.   Display start line is set at display RAM address 0
//  6.   Column address counter is set at 0
//  7.   Normal scan direction of the COM outputs
//  8.   Contrast control register is set at 7Fh
//  9.   Normal display mode (Equivalent to A4h command)
bool PMOD::PmodOLED::reset()
{
    bool ret = true;

    ret &= Reset.setHigh();     //digitalWrite(rstPin, HIGH);
    ret &= OSEF::sleepms(1);    //delay(1);                   // VDD goes high at start, pause for 1 ms
    ret &= Reset.setLow();      //digitalWrite(rstPin, LOW);  // Bring reset low
    ret &= OSEF::sleepms(10);   //delay(10);                  // Wait 10 ms
    ret &= Reset.setHigh();     //digitalWrite(rstPin, HIGH); // Bring out of reset

    return ret;
}

bool PMOD::PmodOLED::logicOn()
{
    const bool ret = Vdd.setHigh();

    if(ret)
    {
        _DOUT("Logic voltage ON")
    }
    else
    {
        _DERR("Failed to set logic voltage ON");
    }

    return ret;
}

bool PMOD::PmodOLED::logicOff()
{
    const bool ret = Vdd.setLow();

    if(ret)
    {
        _DOUT("Logic voltage OFF")
    }
    else
    {
        _DERR("Failed to set logic voltage OFF");
    }

    return ret;
}

bool PMOD::PmodOLED::batteryOn()
{
    const bool ret = Vbat.setHigh();

    if(ret)
    {
        _DOUT("Battery voltage ON")
    }
    else
    {
        _DERR("Failed to set Battery voltage ON");
    }

    return ret;
}

bool PMOD::PmodOLED::batteryOff()
{
    const bool ret = Vbat.setLow();

    if(ret)
    {
        _DOUT("Battery voltage OFF")
    }
    else
    {
        _DERR("Failed to set Battery voltage OFF");
    }

    return ret;
}

bool PMOD::PmodOLED::getPmodOledMapping(   int argc, char** argv, uint32_t& periph, uint32_t& cs
                                ,uint32_t& dc, uint32_t& vdd, uint32_t& vbat, uint32_t& rst)
{
    bool ret = true;
    uint64_t tmp = 0;

    if(argc>1)
    {
	/*use function getopt to get the arguments with option."hu:p:s:v" indicate
	that option h,v are the options without arguments while u,p,s are the
	options with arguments*/
        int32_t opt = getopt(argc,argv,"p:c:d:v:b:r:");
	while(opt!=-1)
	{
	    switch(opt)
	    {
		case 'p':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffUL)
                    {
                        periph = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
		case 'c':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffUL)
                    {
                        cs = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
                case 'd':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffffUL)
                    {
                        dc = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
                case 'v':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffffUL)
                    {
                        vdd = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
                case 'b':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffffUL)
                    {
                        vbat = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
                case 'r':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffffUL)
                    {
                        rst = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
		default:
                    ret = false;
                    break;
	    }

            opt = getopt(argc,argv,"p:c:");
	}
    }

    if(!ret)
    {
        std::cout<<"Usage:   "<<argv[static_cast<size_t>(0)]<<" [-option] [argument]"<<std::endl;
        std::cout<<"option:  "<<std::endl;
        std::cout<<"         "<<"-p  SPI peripheral [0-255]"<<std::endl;
        std::cout<<"         "<<"-c  SPI chip select [0-255]"<<std::endl;
        std::cout<<"         "<<"-d  D/C GPIO pin [0-65535]"<<std::endl;
        std::cout<<"         "<<"-v  Vdd GPIO pin [0-65535]"<<std::endl;
        std::cout<<"         "<<"-b  Vbat GPIO pin [0-65535]"<<std::endl;
        std::cout<<"         "<<"-r  Reset GPIO pin [0-65535]"<<std::endl;
    }

    return ret;
}
